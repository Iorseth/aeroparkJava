package aeroport;

import com.mongodb.MongoClient;
import domain.Aeroport;
import domain.MongoDBAeroportDAO;

import java.util.List;

public class readAllAeroport {
    public static void  main(String args[]) {

        MongoClient mongo = new MongoClient();
        MongoDBAeroportDAO mdao = new MongoDBAeroportDAO(mongo);

        List<Aeroport> test = mdao.readAllAeroport();
        for (Aeroport aeroport : test) {
            System.out.println(aeroport.getNbParking());
        }
    }
}
