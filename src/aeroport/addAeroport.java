package aeroport;

        import com.mongodb.MongoClient;
        import domain.Aeroport;
        import domain.MongoDBAeroportDAO;

public class addAeroport {
    public static void  main(String args[]) {

        MongoClient mongo = new MongoClient();
        MongoDBAeroportDAO mdao = new MongoDBAeroportDAO(mongo);

        Aeroport test = new Aeroport();
        test.setNbParking(3);
        test.setDescription("Ma nouvelle description");
        test.setName("BGAIRPORT2");
        mdao.createAeroport(test);
    }
}
