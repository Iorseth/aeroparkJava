package aeroport;

import com.mongodb.MongoClient;
import domain.Aeroport;
import domain.MongoDBAeroportDAO;

import java.util.List;

public class deleteAeroport {
    public static void  main(String args[]) {

        MongoClient mongo = new MongoClient();
        MongoDBAeroportDAO mdao = new MongoDBAeroportDAO(mongo);

        List<Aeroport> test = mdao.readAllAeroport();
        Aeroport aeroport = test.get(0);
        mdao.deleteAeroport(aeroport);
    }
}