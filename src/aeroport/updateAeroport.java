package aeroport;

import com.mongodb.MongoClient;
import domain.Aeroport;
import domain.MongoDBAeroportDAO;

import java.util.List;

public class updateAeroport {
    public static void  main(String args[]) {

        MongoClient mongo = new MongoClient();
        MongoDBAeroportDAO mdao = new MongoDBAeroportDAO(mongo);

        List<Aeroport> test = mdao.readAllAeroport();
        for (Aeroport aeroport : test) {
            aeroport.setNbParking(2);
            String aeroportName = aeroport.getName();
            aeroport.setName(aeroportName);
            mdao.updateAeroport(aeroport);
        }
    }
}