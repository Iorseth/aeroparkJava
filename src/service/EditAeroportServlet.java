package service;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.MongoDBAeroportDAO;
import domain.Aeroport;
import com.mongodb.MongoClient;

@WebServlet("/editAeroport")
public class EditAeroportServlet extends HttpServlet {

    private static final long serialVersionUID = -6554920927964049383L;

    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        if (id == null || "".equals(id)) {
            throw new ServletException("id missing for edit operation");
        }
        System.out.println("Aeroport edit requested with id=" + id);
        MongoClient mongo = (MongoClient) request.getServletContext()
                .getAttribute("MONGO_CLIENT");
        MongoDBAeroportDAO AeroportDAO = new MongoDBAeroportDAO(mongo);
        Aeroport p = new Aeroport();
        p.setId(id);
        p = AeroportDAO.readAeroport(p);
        request.setAttribute("Aeroport", p);
        List<Aeroport> Aeroports = AeroportDAO.readAllAeroport();
        request.setAttribute("Aeroports", Aeroports);

        RequestDispatcher rd = getServletContext().getRequestDispatcher(
                "/Aeroports.jsp");
        rd.forward(request, response);
    }

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id"); // keep it non-editable in UI
        if (id == null || "".equals(id)) {
            throw new ServletException("id missing for edit operation");
        }

        String name = request.getParameter("name");
        String description = request.getParameter("description");
        int nbParking = Integer.parseInt(request.getParameter("nbParking"));

        if ((name == null || name.equals(""))
                || (description == null || description.equals(""))
                || (nbParking == 0)) {
            request.setAttribute("error", "Name and description Can't be empty");
            MongoClient mongo = (MongoClient) request.getServletContext()
                    .getAttribute("MONGO_CLIENT");
            MongoDBAeroportDAO AeroportDAO = new MongoDBAeroportDAO(mongo);
            Aeroport p = new Aeroport();
            p.setId(id);
            p.setName(name);
            p.setDescription(description);
            p.setNbParking(nbParking);
            request.setAttribute("Aeroport", p);
            List<Aeroport> Aeroports = AeroportDAO.readAllAeroport();
            request.setAttribute("Aeroports", Aeroports);

            RequestDispatcher rd = getServletContext().getRequestDispatcher(
                    "/Aeroports.jsp");
            rd.forward(request, response);
        } else {
            MongoClient mongo = (MongoClient) request.getServletContext()
                    .getAttribute("MONGO_CLIENT");
            MongoDBAeroportDAO AeroportDAO = new MongoDBAeroportDAO(mongo);
            Aeroport p = new Aeroport();
            p.setId(id);
            p.setName(name);
            p.setDescription(description);
            p.setNbParking(nbParking);
            AeroportDAO.updateAeroport(p);
            System.out.println("Aeroport edited successfully with id=" + id);
            request.setAttribute("success", "Aeroport edited successfully");
            List<Aeroport> Aeroports = AeroportDAO.readAllAeroport();
            request.setAttribute("Aeroports", Aeroports);

            RequestDispatcher rd = getServletContext().getRequestDispatcher(
                    "/Aeroports.jsp");
            rd.forward(request, response);
        }
    }

}