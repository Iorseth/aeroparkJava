package service;
import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.MongoDBAeroportDAO;
import domain.Aeroport;
import com.mongodb.MongoClient;

            @WebServlet("/addAeroport")
            public class AddAeroportServlet extends HttpServlet {

                private static final long serialVersionUID = -7060758261496829905L;

                protected void doPost(HttpServletRequest request,
                                      HttpServletResponse response) throws ServletException, IOException {
                    String name = request.getParameter("name");
                    String description = request.getParameter("description");
                    int nbParking = Integer.parseInt(request.getParameter("nbParking"));
                    if ((name == null || name.equals(""))
                            || (description == null || description.equals(""))
                            || (nbParking == 0 )) {
                        request.setAttribute("error", "Mandatory Parameters Missing");
                        RequestDispatcher rd = getServletContext().getRequestDispatcher(
                                "/Aeroports.jsp");
                        rd.forward(request, response);
                    } else {
                        Aeroport p = new Aeroport();
                        p.setDescription(description);
                        p.setNbParking(nbParking);
                        p.setName(name);
                        MongoClient mongo = (MongoClient) request.getServletContext()
                                .getAttribute("MONGO_CLIENT");
                        MongoDBAeroportDAO AeroportDAO = new MongoDBAeroportDAO(mongo);
                        AeroportDAO.createAeroport(p);
                        System.out.println("Aeroport Added Successfully with id="+p.getId());
                        request.setAttribute("success", "Aeroport Added Successfully");
                        List<Aeroport> Aeroports = AeroportDAO.readAllAeroport();
                        request.setAttribute("Aeroports", Aeroports);

            RequestDispatcher rd = getServletContext().getRequestDispatcher(
                    "/Aeroports.jsp");
            rd.forward(request, response);
        }
    }

}