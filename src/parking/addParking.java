package parking;

import com.mongodb.MongoClient;
import domain.Parking;
import domain.MongoDBParkingDAO;

public class addParking {
    public static void  main(String args[]) {

        MongoClient mongo = new MongoClient();
        MongoDBParkingDAO mdao = new MongoDBParkingDAO(mongo);

        Parking test = new Parking();
        test.setNbPlace(30000);
        test.setDescription("Ma nouvelle description");
        test.setNom("BGParking");
        test.setAeroport("BEAUVAIS");
        mdao.createParking(test);
    }
}
