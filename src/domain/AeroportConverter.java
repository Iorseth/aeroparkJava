package domain;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;

public class AeroportConverter {

    public static DBObject toDBObject(Aeroport p) {

        BasicDBObjectBuilder builder = BasicDBObjectBuilder.start()
                .append("name", p.getName()).append("description", p.getDescription())
                .append("nbParking", p.getNbParking());
        if (p.getId() != null)
            builder = builder.append("_id", new ObjectId(p.getId()));
        return builder.get();
    }

    // convert DBObject Object to Aeroport
    // take special note of converting ObjectId to String
    public static Aeroport toAeroport(DBObject doc) {
        Aeroport p = new Aeroport();
        p.setName((String) doc.get("name"));
        p.setDescription((String) doc.get("description"));
        p.setNbParking((int) doc.get("nb_parking"));
        ObjectId id = (ObjectId) doc.get("_id");
        p.setId(id.toString());
        return p;
    }
}
