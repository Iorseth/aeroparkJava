package domain;

public class Aeroport {

    private String id;

    private String name;

    private String description;

    private int nbParking;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNbParking() {
        return nbParking;
    }

    public void setNbParking(int nbParking) {
        this.nbParking = nbParking;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
