package domain;

import java.util.Date;

public class Place {
	
	int numPlace;
	int typePlace;
	
	Parking parking;
	Date debutReservation;
	Date finReservation;

	public Parking getParking() {
		return parking;
	}

	public void setParking(Parking parking) {
		this.parking = parking;
	}
	
	public Place(int numPlace) {
		super();
		this.numPlace = numPlace;
	}
	
	public int getNumPlace() {
		return numPlace;
	}

	public void setNumPlace(int numPlace) {
		this.numPlace = numPlace;
	}

	public Date getdebutReservation() {
		return debutReservation;
	}

	public void setdebutReservation(Date debutReservation) {
		this.debutReservation = debutReservation;
	}

	public Date getfinReservation() {
		return finReservation;
	}

	public void setfinReservation(Date finReservation) {
		this.finReservation = finReservation;
	}

	public int gettypePlace() {
		return typePlace;
	}

	public void settypePlace(int typePlace) {
		this.typePlace = typePlace;
	}
	
}
