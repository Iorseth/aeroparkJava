package domain;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

public class MongoDBAeroportDAO {

    private DBCollection col;

    public MongoDBAeroportDAO(MongoClient mongo) {
        this.col = mongo.getDB("aeroPark").getCollection("aeroport");
    }

    public Aeroport createAeroport(Aeroport p) {
        DBObject doc = AeroportConverter.toDBObject(p);
        this.col.insert(doc);
        ObjectId id = (ObjectId) doc.get("_id");
        p.setId(id.toString());
        return p;
    }

    public void updateAeroport(Aeroport p) {
        DBObject query = BasicDBObjectBuilder.start()
                .append("_id", new ObjectId(p.getId())).get();
        this.col.update(query, AeroportConverter.toDBObject(p));
    }

    public List<Aeroport> readAllAeroport() {
        List<Aeroport> data = new ArrayList<Aeroport>();
        DBCursor cursor = col.find();
        while (cursor.hasNext()) {
            DBObject doc = cursor.next();
            Aeroport p = AeroportConverter.toAeroport(doc);
            data.add(p);
        }
        return data;
    }

    public void deleteAeroport(Aeroport p) {
        DBObject query = BasicDBObjectBuilder.start()
                .append("_id", new ObjectId(p.getId())).get();
        this.col.remove(query);
    }

    public Aeroport readAeroport(Aeroport p) {
        DBObject query = BasicDBObjectBuilder.start()
                .append("_id", new ObjectId(p.getId())).get();
        DBObject data = this.col.findOne(query);
        return AeroportConverter.toAeroport(data);
    }

}