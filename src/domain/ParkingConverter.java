package domain;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;

public class ParkingConverter {

    public static DBObject toDBObject(Parking p) {

        BasicDBObjectBuilder builder = BasicDBObjectBuilder.start()
                .append("nom", p.getNom()).append("description", p.getDescription())
                .append("aeroport", p.getAeroport())
                .append("nb_place", p.getNbPlace());
        if (p.getId() != null)
            builder = builder.append("_id", new ObjectId(p.getId()));
        return builder.get();
    }

    // convert DBObject Object to Parking
    // take special note of converting ObjectId to String
    public static Parking toParking(DBObject doc) {
        Parking p = new Parking();
        p.setNom((String) doc.get("nom"));
        p.setDescription((String) doc.get("description"));
        p.setAeroport((String) doc.get("aeroport"));
        p.setNbPlace((int) doc.get("nb_place"));
        ObjectId id = (ObjectId) doc.get("_id");
        p.setId(id.toString());
        return p;
    }
}
