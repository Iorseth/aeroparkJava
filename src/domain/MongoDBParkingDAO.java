package domain;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

public class MongoDBParkingDAO {

    private DBCollection col;

    public MongoDBParkingDAO(MongoClient mongo) {
        this.col = mongo.getDB("aeroPark").getCollection("parking");
    }

    public Parking createParking(Parking p) {
        DBObject doc = ParkingConverter.toDBObject(p);
        this.col.insert(doc);
        ObjectId id = (ObjectId) doc.get("_id");
        p.setId(id.toString());
        return p;
    }

    public void updateParking(Parking p) {
        DBObject query = BasicDBObjectBuilder.start()
                .append("_id", new ObjectId(p.getId())).get();
        this.col.update(query, ParkingConverter.toDBObject(p));
    }

    public List<Parking> readAllParking() {
        List<Parking> data = new ArrayList<Parking>();
        DBCursor cursor = col.find();
        while (cursor.hasNext()) {
            DBObject doc = cursor.next();
            Parking p = ParkingConverter.toParking(doc);
            data.add(p);
        }
        return data;
    }

    public void deleteParking(Parking p) {
        DBObject query = BasicDBObjectBuilder.start()
                .append("_id", new ObjectId(p.getId())).get();
        this.col.remove(query);
    }

    public Parking readParking(Parking p) {
        DBObject query = BasicDBObjectBuilder.start()
                .append("_id", new ObjectId(p.getId())).get();
        DBObject data = this.col.findOne(query);
        return ParkingConverter.toParking(data);
    }

}