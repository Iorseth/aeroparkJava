# JAVA DOC


## MONGODB

une fois la database existante, clique droit sur cette dernière ( ici aeropark) et faire "créer collection"

- On indique le nom aeroport

On fait un "insert data" et on entre les documents suivantes ( bien faire attention à effacer tous les caracteres avant copier coller)
Aussi, ces opérations sont à faire autant de fois qu'il y a de documents, donc 3 dans le cas présent.

L'ID de chaque document se generera tout seul sans avoir besoin de le préciser.

### aeroport
le 1er
```
{
    "nom" : "CDG",
    "description" : "Data about CDG ROISSY",
    "tags" : [ 
	"Paris", 
        "ROISSY"
    ],
    "nb_parking" : 1
}

```

Le second

```
{
    "nom" : "ORLY",
    "description" : "Data about ORLY",
    "tags" : [ 
	"Paris", 
        "ORLY"
    ],
    "nb_parking" : 1
}
        
```

Le troisieme

```
{
    "nom" : "BEAUVAIS",
    "description" : "Data about BEAUVAIS",
    "tags" : [ 
	"PICARDIE", 
        "BEAUVAIS"
    ],
    "nb_parking" : 1
}
       
```

### Parking

Voici les fichiers à inserer pour chaque parking de chaque aéroport

BEAUVAIS

```
{
    "aeroport" : "BEAUVAIS",
    "nom" : "principal_beauvais",
    "description" : "Parking principal de l'aeroport de BEAUVAIS",
    "tags" : [ 
        "PICARDIE", 
        "BEAUVAIS"
    ],
    "nb_place" : 500
}

```

ROISSY

```
{
    "aeroport" : "CDG",
    "nom" : "principal_cdg",
    "description" : "Parking principal de l'aeroport de ROISSY-CDG",
    "tags" : [ 
        "PARIS", 
        "ROISSY"
    ],
    "nb_place" : 5000
}
```

ORLY

```
{
    "aeroport" : "ORLY",
    "nom" : "principal_orly",
    "description" : "Parking principal de l'aeroport de ORLY",
    "tags" : [ 
        "PARIS", 
        "ROISSY"
    ],
    "nb_place" : 3500
}

```

### PLACES

L'heure dans ces dates se mettra toute seule

```

{
    "parking" : "principal_orly",
    "nom_client" : "FAYDEL",
    "plaque" : "AA-123-AA",
    "num_place" : 5,
    "date_debut" : ISODate("2017-02-14T22:34:20.201Z"),
    "date_fin" : ISODate("2017-02-22T22:42:20.201Z")
}

```
```
{
    "parking" : "principal_orly",
    "nom_client" : "JEFFERSON",
    "plaque" : "AA-176-TE",
    "num_place" : 10,
    "date_debut" : ISODate("2017-03-14"),
    "date_fin" : ISODate("2017-04-02")
}
```

```

{
    "parking" : "principal_cdg",
    "nom_client" : "DUPONT",
    "plaque" : "AR-953-OP",
    "num_place" : 7,
    "date_debut" : ISODate("2017-01-23"),
    "date_fin" : ISODate("2017-02-01")
}
```

